#include <bluefruit.h>
#include <Wire.h>
#include <bsec.h>
////////////////////////////////////////////////////////////////
// Macro definition
#define IAQ_ACCURACY_THRESHOLD 1 // 0 => sensor calibration in progress : else => sensor ready
#define IAQ_INDEX 0
#define VOC_INDEX 1
#define SENSOR_DEFAULT 0
////////////////////////////////////////////////////////////////
// Enumrations
typedef enum print_log{
  SENSOR_CONFIG,
  SENSOR_FAILURE,
  SYSTEM_METRICS,
  SENSOR_DATA
} log_info;
typedef enum led_config{
  INIT,
  SENSOR_RUNNING,
  SENSOR_FAILURE_LED
} led_state;
////////////////////////////////////////////////////////////////
// Structures
typedef struct ATTR_PACKED{
//  uint8_t flags[3] = {0x02,0x01,0x06};
//  uint8_t name[5] = {0x05,0x09,'T','A','G'};
//  uint8_t uuid[4] ={0x03,0x03,'2','4'};
//  uint8_t manufacutrer_data[4] = {0x0e,0xff,0xff,0xff};
  float temperature; // Indoor temperature
  float humidity;     // Indoor humidity
  uint16_t iaq;       // Indoor Air Quality
  float voc;          // Volatile Organic Compound (Gas metric)
} ble_adv_payload;
typedef struct{
  ble_adv_payload payload;
  bool valid_iaq; // False => IAQ value unstable : else IAQ value stable
} air_quality_metrics_t;
////////////////////////////////////////////////////////////////
// Local Varibales
BLEUuid uuid(0x8291);           // unique id for the Bluetooth
Bsec iaqSensor;                 // Create an instance of Bsec (BME680 sensor)
String output;                  // string to print on serial port
bool updated_payload_available; // flag to poll for monitoring updated payload 0 => no new data available : else => new data available for transmission
air_quality_metrics_t air_quality_metrics;
////////////////////////////////////////////////////////////////
// Local functions declaration
void ble_update_advertisement(void);
void sensor_config();                         // Initialize BME680 sensor
void update_payload();                        // Update payload values
void update_led(led_state current_led_state); // Update LED status
void print_log_info(log_info log_data);       // Print log data on serial monitor
////////////////////////////////////////////////////////////////
// Setup
void setup(){
  Serial.begin(115200);
  Wire.begin();
  Serial.println("creator's TAG with RAK4630");
  Serial.println("-------------------------------------\n");
  update_led(INIT);
  sensor_config();
  print_log_info(SYSTEM_METRICS);
  // Initialize Bluefruit with max concurrent connections as Peripheral = 1, Central = 1
  // SRAM usage required by SoftDevice will increase with number of connections
  Bluefruit.begin(1, 1);
  Bluefruit.setTxPower(4); // Check bluefruit.h for supported values
  Bluefruit.setName("TAG");
  ble_update_advertisement();
}
////////////////////////////////////////////////////////////////
// Loop
void loop(){
  if (iaqSensor.status == BSEC_OK){
    if (iaqSensor.run()){ 
      // If new data is available
      update_led(SENSOR_RUNNING);
      update_payload();
      print_log_info(SENSOR_DATA);
      ble_update_advertisement();
    }
  }else{ // Sensor failure condition
    print_log_info(SENSOR_FAILURE);
    update_led(SENSOR_FAILURE_LED);
  }
}
////////////////////////////////////////////////////////////////
// Update Advertisement
void ble_update_advertisement(void){
  if (Bluefruit.Advertising.isRunning()){
    Bluefruit.Advertising.stop();
  }
  Bluefruit.Advertising.clearData();
  Bluefruit.Advertising.addFlags(BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE);
  Bluefruit.Advertising.addUuid(0xffff);
  Bluefruit.Advertising.addName();
  Bluefruit.Advertising.addData(BLE_GAP_AD_TYPE_MANUFACTURER_SPECIFIC_DATA,(uint8_t*)&(air_quality_metrics.payload), sizeof(air_quality_metrics.payload));
  Bluefruit.Advertising.setInterval(1600, 1600); // in unit of 0.625 ms
//  Bluefruit.Advertising.setStopCallback(ble_update_advertisement);
  Bluefruit.Advertising.restartOnDisconnect(true);
  Bluefruit.Advertising.setType(BLE_GAP_ADV_TYPE_NONCONNECTABLE_SCANNABLE_UNDIRECTED);
  Bluefruit.Advertising.start();
}
void sensor_config()
{
  iaqSensor.begin(BME680_I2C_ADDR_PRIMARY, Wire);

  print_log_info(SENSOR_CONFIG);

  bsec_virtual_sensor_t sensorList[10] = {
      BSEC_OUTPUT_RAW_TEMPERATURE,
      BSEC_OUTPUT_RAW_PRESSURE,
      BSEC_OUTPUT_RAW_HUMIDITY,
      BSEC_OUTPUT_RAW_GAS,
      BSEC_OUTPUT_IAQ,
      BSEC_OUTPUT_STATIC_IAQ,
      BSEC_OUTPUT_CO2_EQUIVALENT,
      BSEC_OUTPUT_BREATH_VOC_EQUIVALENT,
      BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_TEMPERATURE,
      BSEC_OUTPUT_SENSOR_HEAT_COMPENSATED_HUMIDITY,
  };

  iaqSensor.updateSubscription(sensorList, 10, BSEC_SAMPLE_RATE_LP);
}

void update_payload()
{
  air_quality_metrics.payload.temperature = iaqSensor.temperature;
  air_quality_metrics.payload.humidity = iaqSensor.humidity;
  air_quality_metrics.payload.iaq = iaqSensor.iaq;
  air_quality_metrics.payload.voc = iaqSensor.breathVocEquivalent;
  air_quality_metrics.valid_iaq = iaqSensor.iaqAccuracy >= IAQ_ACCURACY_THRESHOLD ? true : false;
}
void update_led(led_state current_led_state)
{
  switch (current_led_state)
  {
  case INIT:
    pinMode(LED_GREEN, OUTPUT);
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, LOW);
    break;
  case SENSOR_RUNNING:
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_BLUE, LOW);
    break;
  case SENSOR_FAILURE_LED:
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, HIGH);
    break;
  default:
    digitalWrite(LED_GREEN, LOW);
    digitalWrite(LED_BLUE, LOW);
  }
}

void print_log_info(log_info log_data)
{
  switch (log_data)
  {
  case SENSOR_CONFIG:
    output = "\nBSEC library version " + String(iaqSensor.version.major) + "." + String(iaqSensor.version.minor) + "." + String(iaqSensor.version.major_bugfix) + "." + String(iaqSensor.version.minor_bugfix);
    Serial.println(output);
    break;
  case SENSOR_FAILURE:
    output = "BSEC error code : " + String(iaqSensor.status);
    Serial.println(output);
    break;
  case SYSTEM_METRICS:
    output = "System metrics: Temperature, Humidity, IAQ, VOC";
    Serial.println(output);
    break;
  case SENSOR_DATA:
    output = "Temperature: " + String(iaqSensor.temperature);
    output += ", Humidity: " + String(iaqSensor.humidity);
    output += ", IAQ: " + String(iaqSensor.iaq);
    output += ", IAQAccuracy: " + String(iaqSensor.iaqAccuracy);
    output += ", VOC: " + String(iaqSensor.breathVocEquivalent);
    Serial.println(output);
    break;
  default:
    output = "No valid log data";
    Serial.println(output);
  }
}